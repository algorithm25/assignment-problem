#include <bits/stdc++.h>

using namespace std;

int main() {
    int mat[4][4] = {{9, 3, 7, 9},      // matrix represting cost
                   {6, 4, 2, 5},
                   {5, 4, 1, 8},
                   {7, 6, 7, 6}};
    vector<char> job = {'1', '2', '3', '4'};  // jobs
    vector<char> person = {'1', '2', '3', '4'};  //person
    vector<int> perm = {0, 1, 2, 3};   // just the indexing of person for next_permutation
    int mn = INT_MAX;  // store min cost
    vector<int> ans;  // store the min matching
    do {
        int tmp = 0;  // calculate sum
        for (int i = 0; i < 4; i++) {
            tmp += mat[i][perm[i]];
        }
        if (mn > tmp) {   // if we got new min cost change cost and matching
            mn = tmp;
            ans = perm;
        }

    } while (next_permutation(perm.begin(),
                              perm.end()));      // generate all permuations of perm eg  0,1,2,3 then 0,1,3,2 

    cout << "MIN COST: " << mn << endl;
    cout << "ALLOCATION:" << endl;
    for (int i = 0; i < 4; i++) {
        cout  <<"Person "<< person[ans[i]]<< "->"<<"Job "<< job[i]  << endl;          // output matching
    }
    return 0;
}